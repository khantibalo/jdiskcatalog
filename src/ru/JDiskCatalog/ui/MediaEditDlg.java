package ru.JDiskCatalog.ui;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFileChooser;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import ru.JDiskCatalog.util.SqlUtil;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class MediaEditDlg extends javax.swing.JDialog {
	private JLabel jLabel1;
	private JLabel jLabel2;
	private JTextField ctlMediaName;
	private JTextField ctlPath;
	private JButton ctlOK;
	private JButton ctlCancel;
	private JButton ctlBrowse;
	private int m_nMediaID;	
	private Connection m_objConnection;
	public boolean DialogSuccess;
	
	
	public MediaEditDlg(JFrame frame,Connection objConnection) {
		super(frame);
		m_objConnection=objConnection;
		initGUI();
	}
	
	public MediaEditDlg(JFrame frame,int MediaID,Connection objConnection) {
		super(frame);
		m_objConnection=objConnection;
		m_nMediaID=MediaID;
		initGUI();
	}
	
	private void initGUI() {
		try {
			{
				getContentPane().setLayout(null);
				this.setModal(true);
				this.setResizable(false);
				this.setTitle("Edit media");
			}
			{
				jLabel1 = new JLabel();
				getContentPane().add(jLabel1, "Center");
				jLabel1.setText("Name:");
				jLabel1.setBounds(12, 19, 59, 22);
			}
			{
				jLabel2 = new JLabel();
				getContentPane().add(jLabel2);
				jLabel2.setText("Path:");
				jLabel2.setBounds(12, 53, 59, 15);
			}
			{
				ctlMediaName = new JTextField();
				getContentPane().add(ctlMediaName);
				ctlMediaName.setBounds(67, 19, 314, 22);
			}
			{
				ctlPath = new JTextField();
				getContentPane().add(ctlPath);
				ctlPath.setBounds(67, 50, 204, 22);
			}
			{
				ctlBrowse = new JButton();
				getContentPane().add(ctlBrowse);
				ctlBrowse.setText("Browse...");
				ctlBrowse.setBounds(277, 50, 104, 22);
				ctlBrowse.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						ctlBrowseActionPerformed(evt);
					}
				});
			}
			{
				ctlOK = new JButton();
				getContentPane().add(ctlOK);
				ctlOK.setText("OK");
				ctlOK.setBounds(145, 83, 101, 28);
				ctlOK.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						ctlOKActionPerformed(evt);
					}
				});
			}
			{
				ctlCancel = new JButton();
				getContentPane().add(ctlCancel);
				ctlCancel.setText("Cancel");
				ctlCancel.setBounds(270, 83, 101, 28);
				ctlCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						ctlCancelActionPerformed(evt);
					}
				});
			}
			this.setSize(390, 152);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void ctlOKActionPerformed(ActionEvent evt) {

		File objFile=new File(ctlPath.getText());
		if(objFile.exists() && objFile.isDirectory())
		{
			PreparedStatement ps=null;
			ResultSet rs=null;
			Statement s=null;
			int MediaID=m_nMediaID;
			
			try
			{			
				if(m_nMediaID>0)
				{
					ps=m_objConnection.prepareStatement("UPDATE MEDIA SET \"NAME\"=?,\"PATH\"=? WHERE MEDIA_ID=?");
					ps.setInt(3, m_nMediaID);
				}
				else
					ps=m_objConnection.prepareStatement("INSERT INTO MEDIA(\"NAME\",\"PATH\") VALUES(?,?)");
				
				ps.setString(1, ctlMediaName.getText());
				ps.setString(2, ctlPath.getText());
				
				ps.execute();
				
				if(m_nMediaID==0)
				{
					s=m_objConnection.createStatement();
					rs=s.executeQuery("SELECT IDENTITY_VAL_LOCAL() FROM MEDIA");
					if(rs.next())
						MediaID=rs.getInt(1);
				}
			}
			catch(SQLException sqle)
			{		
				SqlUtil.ReportSQLException(sqle);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				SqlUtil.CloseResultSet(rs);
				SqlUtil.CloseStatement(s);
				SqlUtil.ClosePreparedStatement(ps);
			}
			
			if(MediaID>0)
			{
	            Object[] options = {"Yes","No"};
	            
	            int nDialogResult = JOptionPane.showOptionDialog((JFrame)getParent(),						
						"This will populate the catalog. Continue?",
						"Confirm",
					    JOptionPane.YES_NO_OPTION,
					    JOptionPane.QUESTION_MESSAGE,
					    null,     //do not use a custom Icon
					    options,  //the titles of buttons
					    options[0]);
				
	            if(nDialogResult==JOptionPane.YES_OPTION)
	            {    
	            	PopulateCatalogDlg objDlg=new PopulateCatalogDlg((JFrame)getParent(),MediaID,m_objConnection);
	            	objDlg.setLocationRelativeTo(null);
	            	objDlg.setVisible(true);        	
	            }
				
	    		DialogSuccess=true;
				setVisible(false);
			}
		}
		else
			JOptionPane.showMessageDialog((JFrame)getParent(),
					"directory does not exist",
				    "error",
				    JOptionPane.ERROR_MESSAGE);
	}
	
	private void ctlCancelActionPerformed(ActionEvent evt) {
		DialogSuccess=false;
		setVisible(false);
	}
	
	private void ctlBrowseActionPerformed(ActionEvent evt) {
		final JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = fc.showOpenDialog(this);
		if(returnVal == JFileChooser.APPROVE_OPTION)
			ctlPath.setText(fc.getSelectedFile().getPath());				
	}		
}
