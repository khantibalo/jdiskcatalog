package ru.JDiskCatalog.ui;

import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.VetoableChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;

import ru.JDiskCatalog.entities.DirectoryInfo;
import ru.JDiskCatalog.entities.FileInfo;
import ru.JDiskCatalog.entities.MediaInfo;
import ru.JDiskCatalog.entities.SearchResultInfo;
import ru.JDiskCatalog.ui.CatalogBrowser.FileListTableModel;
import ru.JDiskCatalog.util.DateValueCellRenderer;
import ru.JDiskCatalog.util.LongValueCellRenderer;
import ru.JDiskCatalog.util.SqlUtil;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class CatalogSearch extends JSplitPane {
	private JScrollPane ctlFileListScroll;
	private JComboBox ctlCommentConjunction;
	private JComboBox ctlSizeConjunction;
	private JComboBox ctlExtensionConjunction;
	private JTextField ctlCommentFilter;
	private JComboBox ctlCommentFilterType;
	private JLabel jLabel4;
	private JLabel jLabel5;
	private JButton ctlSearch;
	private JTextField ctlDateCreatedTo;
	private JLabel ctlDateCreatedToLabel;
	private JTextField ctlDateCreatedFrom;
	private JComboBox ctlMediaFilter;
	private JLabel jLabel6;
	private JComboBox ctlMediaConjunction;
	private JComboBox ctlDateCreatedFilterType;
	private JComboBox ctlExtensionFilter;
	private JSpinner ctlFileSizeTo;
	private JLabel ctlFileSizeToLabel;
	private JSpinner ctlFileSizeFrom;
	private JComboBox ctlSizeFilterType;
	private JLabel jLabel3;
	private JLabel jLabel2;
	private JTextField ctlFileNameFilter;
	private JComboBox ctlFileNameFilterType;
	private JLabel jLabel1;
	private JPanel ctlSearchPanel;
	private JTable ctlFilesList;
	private Connection m_objConnection;
	
	public CatalogSearch(Connection objConnection)
	{
		super();
		m_objConnection=objConnection;
		InitGUI();
	}
	
	private void InitGUI()
	{
		try		
		{
			{
				ctlSearchPanel=new JPanel();
				add(ctlSearchPanel, JSplitPane.TOP);
				ctlSearchPanel.setLayout(null);
				ctlSearchPanel.setPreferredSize(new java.awt.Dimension(730, 199));
				
				FilterMode[] arrConjunctions=new FilterMode[2];
				arrConjunctions[0]=new FilterMode(ConjunctionType.And.ordinal(),"AND");
				arrConjunctions[1]=new FilterMode(ConjunctionType.Or.ordinal(),"OR");
				
				{
					jLabel1 = new JLabel();
					ctlSearchPanel.add(jLabel1);
					jLabel1.setText("file (dir) name:");
					jLabel1.setBounds(96, 12, 100, 15);
				}
				{
					FilterMode[] arrFilterModes=new FilterMode[3];
					arrFilterModes[0]=new FilterMode(TextFilterMode.Contains.ordinal(),"contains");
					arrFilterModes[1]=new FilterMode(TextFilterMode.BeginsWith.ordinal(),"begins with");
					arrFilterModes[2]=new FilterMode(TextFilterMode.Equals.ordinal(),"equals");
					
					ComboBoxModel ctlFileNameFilterTypeModel = 
						new DefaultComboBoxModel(arrFilterModes);
					ctlFileNameFilterType = new JComboBox();
					ctlSearchPanel.add(ctlFileNameFilterType);
					ctlFileNameFilterType.setModel(ctlFileNameFilterTypeModel);
					ctlFileNameFilterType.setBounds(248, 5, 109, 22);
				}
				{
					ctlFileNameFilter = new JTextField();
					ctlSearchPanel.add(ctlFileNameFilter);
					ctlFileNameFilter.setBounds(369, 5, 289, 22);
				}
				{
					jLabel2 = new JLabel();
					ctlSearchPanel.add(jLabel2);
					jLabel2.setText("extension:");
					jLabel2.setBounds(96, 67, 100, 15);
				}
				{
					Object[] arrExtensions=getExtensionsList();
					ComboBoxModel ctlExtensionFilterModel = new DefaultComboBoxModel(arrExtensions);
					ctlExtensionFilter = new JComboBox();
					ctlSearchPanel.add(ctlExtensionFilter);
					ctlExtensionFilter.setModel(ctlExtensionFilterModel);
					ctlExtensionFilter.setBounds(369, 64, 289, 22);
					ctlExtensionFilter.setEditable(true);
				}
				{
					jLabel3 = new JLabel();
					ctlSearchPanel.add(jLabel3);
					jLabel3.setText("size (in bytes):");
					jLabel3.setBounds(93, 98, 90, 15);
				}
				{
					FilterMode[] arrFilterModes=new FilterMode[4];
					arrFilterModes[0]=new FilterMode(FileSizeFilterMode.Equal.ordinal(),"=");
					arrFilterModes[1]=new FilterMode(FileSizeFilterMode.Less.ordinal(),"<");
					arrFilterModes[2]=new FilterMode(FileSizeFilterMode.More.ordinal(),">");
					arrFilterModes[3]=new FilterMode(FileSizeFilterMode.Between.ordinal(),"between");
					
					ComboBoxModel ctlSizeFilterTypeModel = 
						new DefaultComboBoxModel(arrFilterModes);
					ctlSizeFilterType = new JComboBox();
					ctlSearchPanel.add(ctlSizeFilterType);
					ctlSizeFilterType.setModel(ctlSizeFilterTypeModel);
					ctlSizeFilterType.setBounds(248, 95, 109, 22);
					ctlSizeFilterType.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							ctlSizeFilterTypeActionPerformed(evt);
						}
					});
				}
				{
					SpinnerNumberModel ctlFileSizeFromModel = new SpinnerNumberModel(-1,-1,null,1);
					ctlFileSizeFrom = new JSpinner();
					ctlSearchPanel.add(ctlFileSizeFrom);
					ctlFileSizeFrom.setModel(ctlFileSizeFromModel);
					ctlFileSizeFrom.setBounds(369, 96, 99, 22);
				}
				{
					ctlFileSizeToLabel = new JLabel();
					ctlSearchPanel.add(ctlFileSizeToLabel);
					ctlFileSizeToLabel.setText("to");
					ctlFileSizeToLabel.setBounds(479, 97, 32, 19);
					ctlFileSizeToLabel.setVisible(false);
				}
				{
					SpinnerNumberModel ctlFileSizeToModel = new SpinnerNumberModel(-1,-1,null,1);
					ctlFileSizeTo = new JSpinner();
					ctlSearchPanel.add(ctlFileSizeTo);
					ctlFileSizeTo.setModel(ctlFileSizeToModel);
					ctlFileSizeTo.setBounds(551, 96, 107, 22);
					ctlFileSizeTo.setVisible(false);
				}
				{
					jLabel5 = new JLabel();
					ctlSearchPanel.add(jLabel5);
					jLabel5.setText("date created:");
					jLabel5.setBounds(0, 168, 96, 19);
					jLabel5.setVisible(false);
				}
				{														
					ComboBoxModel ctlDateCreatedFilterTypeModel = 
						new DefaultComboBoxModel(
								new String[] { "Item One", "Item Two" });
					ctlDateCreatedFilterType = new JComboBox();
					ctlSearchPanel.add(ctlDateCreatedFilterType);
					ctlDateCreatedFilterType.setModel(ctlDateCreatedFilterTypeModel);
					ctlDateCreatedFilterType.setBounds(96, 168, 96, 19);
					ctlDateCreatedFilterType.setVisible(false);
				}
				{
					ctlDateCreatedFrom = new JTextField();
					ctlSearchPanel.add(ctlDateCreatedFrom);
					ctlDateCreatedFrom.setBounds(226, 168, 96, 19);
					ctlDateCreatedFrom.setVisible(false);
				}
				{
					ctlDateCreatedToLabel = new JLabel();
					ctlSearchPanel.add(ctlDateCreatedToLabel);
					ctlDateCreatedToLabel.setText("to");
					ctlDateCreatedToLabel.setBounds(325, 168, 25, 19);
					ctlDateCreatedToLabel.setVisible(false);
				}
				{
					ctlDateCreatedTo = new JTextField();
					ctlSearchPanel.add(ctlDateCreatedTo);
					ctlDateCreatedTo.setBounds(356, 168, 96, 19);
					ctlDateCreatedTo.setVisible(false);
				}
				{
					jLabel4 = new JLabel();
					ctlSearchPanel.add(jLabel4);
					jLabel4.setText("comment:");
					jLabel4.setBounds(93, 130, 77, 15);

					FilterMode[] arrFilterModes=new FilterMode[3];
					arrFilterModes[0]=new FilterMode(TextFilterMode.Contains.ordinal(),"contains");
					arrFilterModes[1]=new FilterMode(TextFilterMode.BeginsWith.ordinal(),"begins with");
					arrFilterModes[2]=new FilterMode(TextFilterMode.Equals.ordinal(),"equals");
					
					ComboBoxModel ctlCommentFilterTypeModel = new DefaultComboBoxModel(arrFilterModes);
					ctlCommentFilterType = new JComboBox();
					ctlSearchPanel.add(ctlCommentFilterType);
					ctlCommentFilterType.setModel(ctlCommentFilterTypeModel);
					ctlCommentFilterType.setBounds(248, 126, 109, 22);

					ctlCommentFilter = new JTextField();
					ctlSearchPanel.add(ctlCommentFilter);
					ctlCommentFilter.setBounds(369, 127, 289, 22);
				}		
				{
					ComboBoxModel ctlExtensionConjunctionModel = new DefaultComboBoxModel(arrConjunctions);
					ctlExtensionConjunction = new JComboBox();
					ctlSearchPanel.add(ctlExtensionConjunction);
					ctlExtensionConjunction.setModel(ctlExtensionConjunctionModel);
					ctlExtensionConjunction.setBounds(13, 63, 71, 22);

					ComboBoxModel ctlSizeConjunctionModel =new DefaultComboBoxModel(arrConjunctions);
					ctlSizeConjunction = new JComboBox();
					ctlSearchPanel.add(ctlSizeConjunction);
					ctlSizeConjunction.setModel(ctlSizeConjunctionModel);
					ctlSizeConjunction.setBounds(13, 98, 71, 22);

					ComboBoxModel ctlCommentConjunctionModel =new DefaultComboBoxModel(arrConjunctions);
					ctlCommentConjunction = new JComboBox();
					ctlSearchPanel.add(ctlCommentConjunction);
					ctlCommentConjunction.setModel(ctlCommentConjunctionModel);
					ctlCommentConjunction.setBounds(13, 126, 71, 22);
				}	
				
						ComboBoxModel ctlFileNameConjuctionModel = new DefaultComboBoxModel(arrConjunctions);
						ctlMediaConjunction = new JComboBox();
						ctlSearchPanel.add(ctlMediaConjunction);
						ctlMediaConjunction.setModel(ctlFileNameConjuctionModel);
						ctlMediaConjunction.setBounds(12, 36, 72, 22);
			
						jLabel6 = new JLabel();
						ctlSearchPanel.add(jLabel6);
						jLabel6.setText("media:");
						jLabel6.setBounds(94, 36, 119, 15);
				
						ComboBoxModel ctlMediaFilterModel = new DefaultComboBoxModel(getMedia());
						ctlMediaFilter = new JComboBox();
						ctlSearchPanel.add(ctlMediaFilter);
						ctlMediaFilter.setModel(ctlMediaFilterModel);
						ctlMediaFilter.setBounds(369, 36, 289, 22);
			
				{
					ctlSearch = new JButton();
					ctlSearchPanel.add(ctlSearch);					
					ctlSearch.setText("Search");
					ctlSearch.setBounds(521, 167, 135, 22);
					ctlSearch.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent evt) {
							ctlSearchActionPerformed(evt);
						}
					});
				}
			}
			{
				ctlFileListScroll = new JScrollPane();
				add(ctlFileListScroll, JSplitPane.BOTTOM);
			}
			
			this.setDividerLocation(200);
			this.setOrientation(JSplitPane.VERTICAL_SPLIT);
			this.setPreferredSize(new java.awt.Dimension(869, 249));
			this.setLastDividerLocation(200);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}	
	}
	
	public Connection getConnection()
	{
		return m_objConnection;
	}
	
	private void ctlSearchActionPerformed(ActionEvent evt) {
		ArrayList<SearchResultInfo> arrFiles=new ArrayList<SearchResultInfo>();
		
		SpinnerNumberModel ctlFileSizeNumberModel = (SpinnerNumberModel)ctlFileSizeFrom.getModel();
		
		if(!ctlFileNameFilter.getText().isEmpty() || ctlExtensionFilter.getSelectedItem()!=null || 
				ctlFileSizeNumberModel.getNumber().intValue()>-1 || ctlDateCreatedFrom.getText()!="")
		{
			ResultSet rs=null;
			ResultSet rsSearchDirs=null;
			PreparedStatement psSearchDirs=null;
			PreparedStatement ps=null;
			
			try
			{	
				setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));	
				
				String strFilter=getFileNameQuery();
						
				if(!strFilter.isEmpty())
				{
					psSearchDirs=m_objConnection.prepareStatement(
							"SELECT FILES.DIRECTORY_ID,MEDIA.NAME AS MEDIANAME,FILES.PATH "+						
							"FROM DIRECTORIES AS FILES "+
							"INNER JOIN MEDIA ON FILES.MEDIA_ID=MEDIA.MEDIA_ID WHERE "+strFilter);	
					
					rsSearchDirs=psSearchDirs.executeQuery();
					
					while(rsSearchDirs.next())
					{					
						arrFiles.add(new SearchResultInfo(0,rsSearchDirs.getInt("DIRECTORY_ID"), rsSearchDirs.getString("MEDIANAME"),
								rsSearchDirs.getString("PATH"),"","",null,null,""));
					}							
				}
				
				String strMediaFilter=getMediaQuery();
				if(!strFilter.isEmpty() && !strMediaFilter.isEmpty())
					strFilter+=getConjunction(ctlMediaConjunction);
				
				strFilter+=strMediaFilter;
				
				String strExtensionsFilter=getExtensionQuery();
				if(!strFilter.isEmpty() && !strExtensionsFilter.isEmpty())
					strFilter+=getConjunction(ctlExtensionConjunction);
				
				strFilter+=strExtensionsFilter;
												
				String strFileSizeFilter=getSizeQuery();
				if(!strFilter.isEmpty() && !strFileSizeFilter.isEmpty())
					strFilter+=getConjunction(ctlSizeConjunction);
				
				strFilter+=strFileSizeFilter;
				
				String strCommentFilter=getCommentQuery();
				if(!strFilter.isEmpty() && !strCommentFilter.isEmpty())
					strFilter+=getConjunction(ctlCommentConjunction);
				
				strFilter+=strCommentFilter;
				
				//TODO: dates filtering
				
				
				ps=m_objConnection.prepareStatement(
						"SELECT FILES.FILE_ID,DIRECTORIES.DIRECTORY_ID,MEDIA.NAME AS MEDIANAME,DIRECTORIES.PATH AS DIRECTORYPATH"+
						",FILES.NAME AS FILENAME,FILES.EXTENSION,FILES.SIZE,FILES.DATECREATED,FILES.COMMENT "+
						"FROM FILES INNER JOIN DIRECTORIES ON FILES.DIRECTORY_ID=DIRECTORIES.DIRECTORY_ID "+
						"INNER JOIN MEDIA ON DIRECTORIES.MEDIA_ID=MEDIA.MEDIA_ID WHERE "+strFilter);						
								
				rs=ps.executeQuery();
				while(rs.next())
				{				
					arrFiles.add(new SearchResultInfo(rs.getInt("FILE_ID"),rs.getInt("DIRECTORY_ID"), rs.getString("MEDIANAME"),
							rs.getString("DIRECTORYPATH"),
							rs.getString("FILENAME"),
							rs.getString("EXTENSION"),
							rs.getLong("SIZE"),
							rs.getLong("DATECREATED"),
							convertStringNulls(rs.getString("COMMENT"))));
				}
			}
			catch(SQLException sqle)
			{		
				SqlUtil.ReportSQLException(sqle);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			finally
			{
				SqlUtil.CloseResultSet(rs);
				SqlUtil.ClosePreparedStatement(ps);
				SqlUtil.ClosePreparedStatement(psSearchDirs);
				
				setCursor(Cursor.getDefaultCursor());
			}	
		}
		
		if(arrFiles.size()>0)
		{
			SearchResultsTableModel objModel=new SearchResultsTableModel(arrFiles);
			ctlFilesList=new JTable(objModel);
			ctlFilesList.setDefaultRenderer(java.lang.Long.class,new LongValueCellRenderer());
			ctlFilesList.setDefaultRenderer(java.util.Date.class,new DateValueCellRenderer());
			ctlFilesList.setAutoCreateRowSorter(true);
			ctlFileListScroll.setViewportView(ctlFilesList);
		}
		else
		{
			ctlFilesList=new JTable(new DefaultTableModel());
			ctlFileListScroll.setViewportView(ctlFilesList);
		}
	}
	
	private Object[] getMedia()
	{
		ArrayList<MediaInfo> arrMediaInfo=new ArrayList<MediaInfo>();
		arrMediaInfo.add(new MediaInfo(-1,"","",0));
		
		Statement s=null;
		ResultSet rs=null;
		
		try
		{
			s=m_objConnection.createStatement();
			rs=s.executeQuery("SELECT MEDIA_ID,\"NAME\" FROM MEDIA");
			
			while(rs.next())
			{
				MediaInfo objMedia=new MediaInfo(rs.getInt("MEDIA_ID"),rs.getString("NAME"),"",0);
				arrMediaInfo.add(objMedia);
			}
		}
		catch(SQLException sqle)
		{		
			SqlUtil.ReportSQLException(sqle);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			SqlUtil.CloseResultSet(rs);
			SqlUtil.CloseStatement(s);
		}
		
		return arrMediaInfo.toArray();
	}
	
	
	private String getConjunction(JComboBox ctlConjunctionControl)
	{
		String strRetVal="";
		
		FilterMode objFilterMode=(FilterMode)ctlConjunctionControl.getSelectedItem();
		
		if(objFilterMode.ModeID==ConjunctionType.And.ordinal())
			strRetVal=" AND ";
		
		if(objFilterMode.ModeID==ConjunctionType.Or.ordinal())
			strRetVal=" OR ";
		
		return strRetVal;
	}
	
	private String convertStringNulls(Object value)
	{
		return value==null ? "" : value.toString();
	}
	
	private void ctlSizeFilterTypeActionPerformed(ActionEvent evt) {
		FilterMode objFilterMode=(FilterMode)ctlSizeFilterType.getSelectedItem();
		
		ctlFileSizeToLabel.setVisible(objFilterMode.ModeID==FileSizeFilterMode.Between.ordinal());
		ctlFileSizeTo.setVisible(objFilterMode.ModeID==FileSizeFilterMode.Between.ordinal());
	}

	private String getFileNameQuery()
	{
		String strRetVal="";
		
		if(!ctlFileNameFilter.getText().isEmpty())		
		{
			FilterMode objFilterMode=(FilterMode)ctlFileNameFilterType.getSelectedItem();
			
			if(objFilterMode.ModeID==TextFilterMode.Contains.ordinal())				
				strRetVal=" UPPER(FILES.NAME) LIKE UPPER('%"+ctlFileNameFilter.getText()+"%') ";
			
			if(objFilterMode.ModeID==TextFilterMode.BeginsWith.ordinal())				
				strRetVal=" UPPER(FILES.NAME) LIKE UPPER('"+ctlFileNameFilter.getText()+"%') ";
			
			if(objFilterMode.ModeID==TextFilterMode.Equals.ordinal())				
				strRetVal=" UPPER(FILES.NAME)='"+ctlFileNameFilter.getText()+"' ";
		}
		
		return strRetVal;
	}
	
	private String getMediaQuery()
	{
		String strRetVal="";
		
		if(((MediaInfo)ctlMediaFilter.getSelectedItem()).mediaID!=-1)
		{
			strRetVal=" MEDIA.MEDIA_ID="+((MediaInfo)ctlMediaFilter.getSelectedItem()).mediaID+" ";			
		}
		
		return strRetVal;
	}	
	
	private String getExtensionQuery()
	{
		String strRetVal="";
		
		if(ctlExtensionFilter.getSelectedItem()!=null && !ctlExtensionFilter.getSelectedItem().toString().isEmpty())
		{			
			String strExtensions=ctlExtensionFilter.getSelectedItem().toString();
			String[] arrExtensions=strExtensions.split(",");
			for(String strExtension : arrExtensions)
			{
				if(!strRetVal.isEmpty())
					strRetVal+=" OR ";
				
				strRetVal+=" UPPER(FILES.EXTENSION)=UPPER('"+strExtension+"')";						
			}	
			
			strRetVal=" ("+ strRetVal+") ";
		}
		
		return strRetVal;
	}
	
	private String getSizeQuery()
	{
		String strRetVal="";
		
		SpinnerNumberModel ctlFileSizeFromNumberModel = (SpinnerNumberModel)ctlFileSizeFrom.getModel();
		if(ctlFileSizeFromNumberModel.getNumber().intValue()>-1)
		{
			FilterMode objFilterMode=(FilterMode)ctlSizeFilterType.getSelectedItem();
			
			if(objFilterMode.ModeID==FileSizeFilterMode.Equal.ordinal())
				strRetVal=" FILES.SIZE="+ctlFileSizeFromNumberModel.getNumber().toString();
			
			if(objFilterMode.ModeID==FileSizeFilterMode.Less.ordinal())
				strRetVal=" FILES.SIZE<"+ctlFileSizeFromNumberModel.getNumber().toString();
			
			if(objFilterMode.ModeID==FileSizeFilterMode.More.ordinal())
				strRetVal=" FILES.SIZE>"+ctlFileSizeFromNumberModel.getNumber().toString();
			
			if(objFilterMode.ModeID==FileSizeFilterMode.Between.ordinal())
			{
				SpinnerNumberModel ctlFileSizeToNumberModel = (SpinnerNumberModel)ctlFileSizeTo.getModel();
				if(ctlFileSizeToNumberModel.getNumber().intValue()>-1)
					strRetVal=" FILES.SIZE BETWEEN "+ctlFileSizeFromNumberModel.getNumber().toString()+" AND "+ctlFileSizeToNumberModel.getNumber().toString();
			}			
		}
		
		return strRetVal;
	}
	
	private String getCommentQuery()
	{
		String strRetVal="";
		
		if(!ctlCommentFilter.getText().isEmpty())		
		{
			FilterMode objFilterMode=(FilterMode)ctlCommentFilterType.getSelectedItem();
			
			if(objFilterMode.ModeID==TextFilterMode.Contains.ordinal())				
				strRetVal=" UPPER(FILES.COMMENT) LIKE UPPER('%"+ctlCommentFilter.getText()+"%') ";
			
			if(objFilterMode.ModeID==TextFilterMode.BeginsWith.ordinal())				
				strRetVal=" UPPER(FILES.COMMENT) LIKE UPPER('"+ctlCommentFilter.getText()+"%') ";
			
			if(objFilterMode.ModeID==TextFilterMode.Equals.ordinal())				
				strRetVal=" UPPER(FILES.COMMENT)='"+ctlCommentFilter.getText()+"' ";
		}
		
		return strRetVal;
	}
	
	
	private Object[] getExtensionsList()
	{
		ArrayList<String> arrStrings=new ArrayList<String>();
		arrStrings.add("");
		
		try
		{
			BufferedReader br = new BufferedReader(new FileReader("extensions.txt"));
			String line;
			while ((line = br.readLine()) != null) {
				if(!line.isEmpty())
					arrStrings.add(line);
			}
			br.close();
		}
		catch(Exception ex)
		{
			
		}
		
		return arrStrings.toArray();
	}
	
	class FilterMode
	{
		public int ModeID;
		public String ModeName;
		
		public FilterMode(int ModeID,String ModeName)
		{
			this.ModeID=ModeID;
			this.ModeName=ModeName;			
		}	
		
		public String toString()
		{
			return ModeName;
		}
	}
	
	enum TextFilterMode
	{
		Contains,
		BeginsWith,
		Equals
	}
	
	enum FileSizeFilterMode
	{
		Equal,
		Less,
		More,
		Between		
	}
	
	enum ConjunctionType
	{
		And,
		Or
	}
	
	class SearchResultsTableModel extends AbstractTableModel
	{
		private String[] _columnNames ={"Media","Directory","File name","Extension","Size","Date created","Comment"};
		private ArrayList<SearchResultInfo> _arrFiles=null;
		
		public SearchResultsTableModel(ArrayList<SearchResultInfo> arrFiles)
		{
			_arrFiles=arrFiles;
		}
		
		@Override
		public int getColumnCount() {
			return _columnNames.length;
		}

		@Override
		public int getRowCount() {
			return _arrFiles.size();
		}

		@Override
		public Object getValueAt(int arg0, int arg1) {
			SearchResultInfo objFileInfo=_arrFiles.get(arg0);
			
			Object objRetVal=null;
			
			switch(arg1)
			{
			case 0:
				objRetVal=objFileInfo.mediaName;
				break;
			case 1:
				objRetVal=objFileInfo.directoryName;
				break;
			case 2:
				objRetVal=objFileInfo.name;
				break;				
			case 3:
				objRetVal=objFileInfo.extension;
				break;
			case 4:
				objRetVal=objFileInfo.size;
				break;
			case 5:
				objRetVal=objFileInfo.getDateCreated();
				break;
			case 6:
				objRetVal=objFileInfo.comment;
				break;				
			}
			
			return objRetVal;
		}
		
		public String getColumnName(int col) {
	        return _columnNames[col];
	    }
		
	    public Class getColumnClass(int c) {	
	    	Class objClassInfo=null;
	    	
	    	switch(c)
	    	{
	    	case 4:
	    		objClassInfo=Long.class;
	    		break;
	    	case 5:
	    		objClassInfo=java.util.Date.class;
	    		break;
	    	default:
	    		objClassInfo=String.class;
	    		break;
	    	}
	    	
	        return objClassInfo;
	    }
	    
	    public boolean isCellEditable(int row, int col) {
	        return col == 6;
	    }
	    
	    public void setValueAt(Object value, int row, int col) {
	    	SearchResultInfo objResult=_arrFiles.get(row);
	    	
	    	if(objResult.directoryId!=0 && objResult.fileId==0)
	    	{//TODO: message box cannot set directory comment
	    		
	    	}
	    	else
	    	{
		    	objResult.comment = value.toString();
		    	
				PreparedStatement ps=null;
				
				try
				{
					ps=m_objConnection.prepareStatement("UPDATE FILES SET COMMENT=? WHERE FILE_ID=?");
					ps.setString(1, value.toString());
					ps.setInt(2, objResult.fileId);
					ps.execute();				
				}
				catch(SQLException sqle)
				{		
					SqlUtil.ReportSQLException(sqle);
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				finally
				{
					SqlUtil.ClosePreparedStatement(ps);
				}	
		    	
		        fireTableCellUpdated(row, col);
	    	}
	    }
	}
}
