package ru.JDiskCatalog.entities;

public class DirectoryInfo 
{
	public int directoryID;
	public int parentID;
	public String name;
	public String path;
	
	public DirectoryInfo(int directoryID, int parentID,String name,String path)
	{
		this.directoryID=directoryID;
		this.parentID=parentID;
		this.name=name;
		this.path=path;
	}
	
	public String toString()
	{
		return name;
	}
}
