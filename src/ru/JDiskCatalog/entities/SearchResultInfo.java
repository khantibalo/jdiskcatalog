package ru.JDiskCatalog.entities;

public class SearchResultInfo {
	public int fileId;
	public int directoryId;
	public String name;
	public String extension;
	public Object size;
	public Object dateCreated;
	public String comment;
	public String directoryName;
	public String mediaName;
	
	public SearchResultInfo(int fileID,int directoryId,String mediaName,String directoryName,String name,String extension,Object size,Object dateCreated,String comment)
	{
		this.directoryId=directoryId;
		this.mediaName=mediaName;
		this.directoryName=directoryName;
		this.fileId=fileID;
		this.name=name;
		this.extension=extension;
		this.size=size;
		this.dateCreated=dateCreated;
		this.comment=comment;
	}		
	
	public Object getDateCreated()
	{
		Object retVal=null;
		if(dateCreated!=null)
		{
			java.util.Date objDate=new java.util.Date();
			objDate.setTime((Long)dateCreated);
			retVal=objDate;
		}
		
		return retVal;
	}
}
