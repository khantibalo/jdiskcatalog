package ru.JDiskCatalog.entities;

public class MediaInfo 
{
	public int mediaID;
	public String name;
	public String path;
	public int rootDirectoryID;
	
	public MediaInfo(int MediaID, String Name, String Path,int RootDirectoryID)
	{
		this.mediaID=MediaID;
		name=Name;
		path=Path;
		rootDirectoryID=RootDirectoryID;
	}		
	
	public String toString()
	{
		return name;
	}
}
