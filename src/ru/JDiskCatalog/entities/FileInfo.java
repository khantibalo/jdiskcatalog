package ru.JDiskCatalog.entities;

import java.sql.Date;

public class FileInfo {
	public int fileID;
	public String name;
	public String extension;
	public long size;
	public long dateCreated;
	public String comment;
	public String directoryName;
	public String mediaName;
	
	public FileInfo(int fileID,String name,String extension,long size,long dateCreated,String comment)
	{
		this.fileID=fileID;
		this.name=name;
		this.extension=extension;
		this.size=size;
		this.dateCreated=dateCreated;
		this.comment=comment;
	}
	
	public FileInfo(int fileID,String mediaName,String directoryName,String name,String extension,long size,long dateCreated,String comment)
	{
		this.mediaName=mediaName;
		this.directoryName=directoryName;
		this.fileID=fileID;
		this.name=name;
		this.extension=extension;
		this.size=size;
		this.dateCreated=dateCreated;
		this.comment=comment;
	}	
	
	public java.util.Date getDateCreated()
	{
		java.util.Date objDate=new java.util.Date();
		objDate.setTime((long)dateCreated);
		return objDate;
	}
}
