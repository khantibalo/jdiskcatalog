package ru.JDiskCatalog.util;

import java.awt.Color;
import java.awt.Component;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class LongValueCellRenderer extends DefaultTableCellRenderer {

	private NumberFormat _sizeFormat;
	
	public LongValueCellRenderer()
	{
		_sizeFormat=NumberFormat.getNumberInstance();
	}
	
    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component c = super.getTableCellRendererComponent(jTable, value, isSelected, hasFocus, row, column);
        if (c instanceof JLabel && value instanceof Long) {
            JLabel label = (JLabel) c;
            label.setHorizontalAlignment(JLabel.RIGHT);
            Number num = (Number) value;
            String text = _sizeFormat.format(num);
            label.setText(text);

            label.setForeground(Color.BLACK);
        }
        return c;
    }
}
