package ru.JDiskCatalog.util;

import java.awt.Color;
import java.awt.Component;
import java.text.DateFormat;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class DateValueCellRenderer extends DefaultTableCellRenderer {
	
	private DateFormat _dateFormat;
	
	public DateValueCellRenderer()
	{
		_dateFormat=DateFormat.getDateTimeInstance();
	}
	
    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component c = super.getTableCellRendererComponent(jTable, value, isSelected, hasFocus, row, column);
        if (c instanceof JLabel && value instanceof java.util.Date) {
            JLabel label = (JLabel) c;
            label.setHorizontalAlignment(JLabel.LEFT);
            java.util.Date date = (java.util.Date) value;
            String text = _dateFormat.format(date);
            label.setText(text);

            label.setForeground(Color.BLACK);
        }
        return c;
    }
}
