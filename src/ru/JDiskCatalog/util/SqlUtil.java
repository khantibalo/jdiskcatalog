package ru.JDiskCatalog.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlUtil 
{	
	public static void ReportSQLException(SQLException sqle)
	{
        while (sqle != null)
        {
        	System.err.println("  Message:    " + sqle.getMessage());
        	sqle=sqle.getNextException();
        }
	}
	
	public static void CloseStatement(Statement s)
	{
        try {
            if (s != null) {
                s.close();
                s = null;
            }
        } catch (SQLException sqle) {
        	SqlUtil.ReportSQLException(sqle);
        }
	}
	
	public static void CloseResultSet(ResultSet rs)
	{
        // ResultSet
        try {
            if (rs != null) {
                rs.close();
                rs = null;
            }
        } catch (SQLException sqle) {
        	SqlUtil.ReportSQLException(sqle);
        }
	}
	
	public static void ClosePreparedStatement(PreparedStatement ps)
	{
        // ResultSet
        try {
            if (ps != null) {
            	ps.close();
            	ps = null;
            }
        } catch (SQLException sqle) {
        	SqlUtil.ReportSQLException(sqle);
        }
	}	
}
